<?php

namespace Drupal\sharepass\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'sharepass.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('sharepass.settings');
//    $form['no_of_failures_before_user_is_ba'] = [
//      '#type' => 'number',
//      '#title' => $this->t('No of failures before user is banned.'),
//      '#default_value' => $config->get('no_of_failures_before_user_is_ba'),
//    ];
//    $form['no_of_failure_before_ip_is_banne'] = [
//      '#type' => 'number',
//      '#title' => $this->t('No of failure before IP is banned.'),
//      '#default_value' => $config->get('no_of_failure_before_ip_is_banne'),
//    ];
    $form['time_for_expire'] = [
      '#type' => 'number',
      '#title' => $this->t('Expiration time for string shared.'),
      '#default_value' => $config->get('time_for_expire'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('sharepass.settings')
      ->set('no_of_failures_before_user_is_ba', $form_state->getValue('no_of_failures_before_user_is_ba'))
      ->set('no_of_failure_before_ip_is_banne', $form_state->getValue('no_of_failure_before_ip_is_banne'))
      ->save();
  }

}
