<?php

namespace Drupal\sharepass\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class Encode.
 */
class Encode extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'encode';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['string_to_encode'] = [
      '#type' => 'textarea',
      '#maxlength' => 255,
      '#title' => $this->t('String to encode'),
      '#weight' => '0',
    ];

//    $form['users'] = [
//      '#type' => 'entity_autocomplete',
//      '#target_type' => 'user',
//      '#tags' => TRUE,
//      '#title' => $this->t('Choose a user, or not.'),
//    ];
//
//    $form['roles'] = [
//      '#type' => 'entity_autocomplete',
//      '#target_type' => 'user_role',
//      '#tags' => TRUE,
//      '#title' => $this->t('Choose a role, or not.'),
//    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {
      // @TODO: Validate fields.
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    $sting = $form_state->getValue('string_to_encode');
    $options = [
//      'users' => $form_state->getValue('users'),
//      'roles' => $form_state->getValue('roles'),
    ];
    $response = \Drupal::service('sharepass.transform')->encode($sting, $options);

    $url = Url::fromRoute('sharepass.decode', [
      'token' => urlencode($response['token']),
      'part' => urlencode($response['part']),
    ]);
    $host = \Drupal::request()->getSchemeAndHttpHost();
    \Drupal::messenger()->addMessage($host . $url->toString());
  }

}
