<?php

namespace Drupal\sharepass;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\PrivateKey;
use Drupal\Core\Database\Connection;

/**
 * Class TransformService.
 */
class TransformService implements TransformInterface {

  const METHOD = 'aes-128-gcm';

  const TAG_HASH = 'sha512';

  const TOKEN_HASH = 'sha256';

  const TABLENAME = 'sharepass';


  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The sharepass config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The current account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * Private key.
   *
   * @var \Drupal\Core\PrivateKey
   */
  protected $privateKey;

  /**
   * Constructs a new TransformService object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AccountProxyInterface $account, PrivateKey $privateKey, Connection $connection) {
    $this->config = $config_factory->get('sharepass.settings');
    $this->account = $account;
    $this->privateKey = $privateKey;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public function decode($token, $part) {

    $this->deleteExpiredTokens();

    $data = $this->getDataByToken($token);
    if (!is_object($data)) {
      return NULL;
    }

    $this->deleteThisToken($token);

    [$cipherText, $key, $iv, $tag] = $this->prepareDecodeData($data, $part);

    return openssl_decrypt($cipherText, self::METHOD, $key, 0, $iv, $tag);
  }

  /**
   * {@inheritdoc}
   */
  public function encode($string, $options) {

    //delete tokens as often as possible
    $this->deleteExpiredTokens();

    if (in_array(self::METHOD, openssl_get_cipher_methods())) {

      [$tag, $iv, $date, $partialCipher, $token, $cipherArray] = $this->prepareEncodeData($string);

      // Store data in db.
      $this->saveData($tag, $iv, $date, $partialCipher, $token, $options);

      return (array) ['token' => $token, 'part' => implode($cipherArray)];
    }

    \Drupal::messenger()->addMessage('Failed to encode!');

    return (array) ['token' => 'invalid', 'part' => 'invalid'];
  }

  private function processOptions($options){

  }

  private function prepareDecodeData($data, $part) {
    $values = get_object_vars($data);
    $cipherText = $values['partial_cipher'] . $part;
    $key = $this->privateKey->get();
    $iv = base64_decode($values['iv']);
    $tag = base64_decode($values['tag']);

    return [$cipherText, $key, $iv, $tag];
  }

  private function prepareEncodeData($string) {

    //Use Drupal private key.
    $key = $this->getPrivateKey();

    $date = time();

    //Build a tag.
    $tag = hash(self::TAG_HASH, $string);

    //Build Initialization Vector
    $ivLen = openssl_cipher_iv_length(self::METHOD);
    $iv = openssl_random_pseudo_bytes($ivLen);

    //TODO: add non-latin support and for special characters
    $cipherText = openssl_encrypt($string, self::METHOD, $key, 0, $iv, $tag);

    //Split encoded string in two parts so one gets to the user and the other is saved
    $split_size = $this->calculateSplitSize(strlen($cipherText));
    $cipherArray = str_split($cipherText, $split_size);
    $partialCipher = $cipherArray[0];
    unset($cipherArray[0]);

    //Build the token
    $token = hash(self::TOKEN_HASH, $string . $date);

    return [$tag, $iv, $date, $partialCipher, $token, $cipherArray];
  }

  private function getPrivateKey() {
    return $this->privateKey->get();
  }

  private function getDataByToken($token) {
    $query = $this->connection->select(self::TABLENAME, 't')
      ->fields('t')
      ->condition('token', $token);
    // Return result as object.
    return $query->execute()->fetchObject();
  }

  /**
   * Delete expired tokens.
   */
  public function deleteExpiredTokens() {
    $this->connection->delete(self::TABLENAME)
      ->condition('date', time() - 5 * 24 * 60 * 60, '<')
      ->execute();
  }

  /**
   * Save data in database.
   */
  private function saveData($tag, $iv, $date, $partialCipher, $token, $options) {
    $this->connection->insert(self::TABLENAME)
      ->fields([
        'tag',
        'iv',
        'date',
        'partial_cipher',
        'token',
        'options',
      ])->values([
        'tag' => base64_encode($tag),
        'iv' => base64_encode($iv),
        'date' => $date,
        'partial_cipher' => $partialCipher,
        'token' => $token,
        'options' => base64_encode(json_encode($options)),
      ])->execute();
  }

  /**
   * Delete expired tokens.
   */
  private function deleteThisToken($token) {
    $this->connection->delete(self::TABLENAME)
      ->condition('token', $token)
      ->execute();
  }

  private function calculateSplitSize($lenght) {
    if ($lenght > 50) {
      $lenght = 50;
    }
    else {
      $lenght = $lenght / 2;
    }

    return $lenght;
  }


}
