<?php

namespace Drupal\sharepass\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class DecodeController.
 */
class DecodeController extends ControllerBase {

  /**
   * Hello.
   *
   * @return string
   *   Return Hello string.
   */
  public function decode($token, $part) {

    $token = urldecode($token);
    $part = urldecode($part);

    $response = \Drupal::service('sharepass.transform')->decode($token, $part);
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Your text: '). $response,
    ];
  }

}
