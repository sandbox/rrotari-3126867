<?php

namespace Drupal\sharepass\Controller;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Zend\Diactoros\Response\JsonResponse;

class AutocompleteController extends ControllerBase {
  public function handleAutocomplete(Request $request){
    $results = [];
    if($input = $request->query->get('q')) {
      $typed_string = Tags::explode($input);
      $typed_string = Unicode::strtolower(array_pop($typed_string));

      $query = \Drupal::database()->select('ol_user_team', 'ost');
      $results = [1, 2, 3, 4, 11, 22, 33, 44, 111, 112, 122, 113, 114];
    }

    return new JsonResponse($results);
  }
}