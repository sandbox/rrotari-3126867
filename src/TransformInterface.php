<?php

namespace Drupal\sharepass;

/**
 * Interface TransformInterface.
 *
 * @package Drupal\sharepass
 */
interface TransformInterface {

  /**
   * Decode a saved string using the part and token
   *
   * @param string $token
   * @param string $part
   *
   * @return string
   */
  public function decode($token, $part);

  /**
   * Encode a string and return a token and partial string
   *
   * @param string $string
   * @param array $options
   *
   * @return array
   */
  public function encode($string, $options);

}
